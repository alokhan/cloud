import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.*;
import org.apache.spark.streaming.*;
import org.apache.spark.streaming.api.java.*;
import org.apache.spark.streaming.kafka.*;
import org.apache.hadoop.conf.Configuration;
import java.util.Arrays;
import java.util.*;
import scala.Tuple2;
import java.io.IOException;
import net.rubyeye.xmemcached.KeyIterator;
import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientBuilder;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.exception.MemcachedException;
import net.rubyeye.xmemcached.utils.AddrUtil;
import java.util.concurrent.TimeoutException;
import net.rubyeye.xmemcached.command.BinaryCommandFactory;

public class TwitterMemcached {

  public static void main(String[] args) throws Exception {
    // Location of the Spark directory
    String sparkHome = "/usr/local/spark";
    // URL of the Spark cluster
    String sparkUrl = "local[4]";
    // Location of the required JAR files
    String jarFile = "target/streaming-1.0.jar";

    JavaStreamingContext ssc = new JavaStreamingContext(sparkUrl, "twitter-alo-memcached", new Duration(6 * 1000), sparkHome, new String[]{jarFile});

    Map<String, Integer> topicMap = new HashMap<String, Integer>();
    topicMap.put("twitter-alo-memcached", new Integer(1));

    JavaPairReceiverInputDStream<String, String> messages = KafkaUtils.createStream(ssc, args[0], args[1], topicMap);

    JavaPairDStream<String, String> tweets = messages.mapToPair(new PairFunction<Tuple2<String,String>, String, String>() {
      public Tuple2<String, String> call(Tuple2<String,String> tuple2) {
        return new Tuple2(tuple2._2().substring(0,24), tuple2._2().substring(25));
      }
    });

    // Split these tweets into words for further processing and map them
    // with the tweet id if they are hashtags.
    JavaPairDStream<String, String>  words = tweets.flatMapToPair(
    new PairFlatMapFunction<Tuple2<String, String>, String,String>() {
      @Override
      public Iterable<Tuple2<String, String>> call(Tuple2<String, String> in) {
        List<Tuple2<String, String>> tuples = new ArrayList<Tuple2<String, String>>();
        for (String word : Arrays.asList(in._2().split(" "))){
          if (word.startsWith("#") && word.length() > 1){
            tuples.add(new Tuple2<String, String>(word, in._1()));
          }
        }
        return tuples;
      }
    }
    );

    // regroup by hashtag all the tweets ID.
    JavaPairDStream<String, Iterable<String>> counts = words.groupByKey();

    // memcached version : VERSION 1.4.14 using BinaryCommand
    try {
      final MemcachedClientBuilder builder = new XMemcachedClientBuilder(AddrUtil.getAddresses("localhost:11211"));
      builder.setCommandFactory(new BinaryCommandFactory());//use binary protocol
      final MemcachedClient memcachedClient = builder.build();
      if (memcachedClient == null) {
        System.out.println("Null MemcachedClient,please check memcached has been started");
      }
      counts.foreach(
      new Function<JavaPairRDD<String, Iterable<String>>, Void> () {
        public Void call(JavaPairRDD<String, Iterable<String>> rdd) {
          for (Tuple2<String, Iterable<String>> t: rdd.take(10)) {
            // TODO: Only keep the 10 last tweets
            try {
              String oldValue = "";
              if (memcachedClient.get(t._1()) != null ){
                oldValue = memcachedClient.get(t._1());
              }
              for (String id : t._2()){
                oldValue += "," + id;
              }
              memcachedClient.set(t._1().toString(),0, oldValue);
              System.out.println(t._1().toString());
              System.out.println(oldValue);
            } catch (Exception e) {
              System.err.println("MemcachedClient operation fail");
            }
          }
          return null;
        }
      }
      );
      ssc.start();

    } catch (Exception e) {
      System.err.println(e);
    }


  }


}
