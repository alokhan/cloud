#!/bin/sh
#argument 1 : path to the jar for spark
pathtojar=target/streaming-1.0.jar
#start zookeeper redirect output to null
echo "Starting zookeeper"
sudo /usr/local/kafka/bin/zookeeper-server-start.sh /usr/local/kafka/config/zookeeper.properties > /dev/null 2>&1 &
sleep 5
#start kafka redirect output to null
echo "Starting kafka"
sudo /usr/local/kafka/bin/kafka-server-start.sh /usr/local/kafka/config/server.properties > /dev/null 2>&1 &
sleep 5
#create topic
echo "creating topic"
sudo /usr/local/kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic twitter-alo-memcached > /dev/null 2>&1 &
#launch spark jar
echo "starting spark"
/usr/local/spark/bin/spark-submit --class TwitterMemcached --master local[4] $pathtojar "localhost:2181" "1"

