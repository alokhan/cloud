var express = require('express');
var assert = require('assert');
var app = require('./app.js');
var ejs = require('ejs');
var fs = require('fs');
var AM = require('./account-manager.js');
var memcached = require("memcached-lite");
var ObjectID = require('mongodb').ObjectID;
var router = express.Router();


////////////////////////////////////////////////////////////////////////////////

function setDisplayDate(tweetsToDisplay) {
    tweetsToDisplay.forEach(function(tweet) {
        tweet.display_time = new Date(tweet.created_at).toString();
    });
    return tweetsToDisplay;
}

// NOTE(norswap): This method is necessary because, annoyingly, EJS does not
//  support dynamic includes (including a file whose name is passed via the
//  dictionary -- it has to be hardcoded in the template instead).
function render(res, dict) {
    fs.readFile('views/'+ dict.partial + '.ejs', 'utf-8', function(err, data) {
        assert(!err);
        dict.partial = ejs.render(data, dict);
        res.render('template', dict);
    });
}

// Diplay the then last posted tweets with the specified hashtag.
// get the data from memcached which have been populated by spark.
router.get('/search/:hashtag', function(req, res){
  var client = memcached("localhost:11211", { retryDelay: 5000, removeTimeout: 20000 });
  client.get("#"+req.params.hashtag, function(err, data, meta) {
    if (data){
      tweets_ids = []
      for(var j = 0; j < data.split(",").length; j++)
      {
        if (data.split(",")[j] != ""){
          tweets_ids.push(new ObjectID(data.split(",")[j]))
        }
      }
      app.tweets.find( { _id: {$in: tweets_ids}}).sort({created_at:-1}).limit(10).toArray(function(err,tweets_from_db){
        render(res, {
          title: "search result for:" ,
          hashtag: req.params.hashtag,
          partial: 'search',
          tweets: setDisplayDate(tweets_from_db)
        });
      });
    }else{
      render(res, {
        title: "search result for:" ,
        hashtag: req.params.hashtag,
        partial: 'search',
        tweets: setDisplayDate([])
      });
    }

  })
});

////////////////////////////////////////////////////////////////////////////////
// Login page

router.get('/', function(req, res) {
  // check if the user's credentials are saved in a cookie
  if (!req.session.user) {
    return res.render('login', {
      message: 'Hello - Please Login To Your Account' });
    }
    // attempt automatic login
    AM.autoLogin(
      req.session.user.username,
      req.session.user.pass,
      app.users,
      function(user) {
        if (!user)
          return res.render('login', {
            title: 'Hello - Please Login To Your Account' });
            req.session.user = user;
            res.redirect('/home');
          });
});

////////////////////////////////////////////////////////////////////////////////
// Login / Logout

router.post('/validate', function(req, res) {
  AM.manualLogin(
    req.body.username,
    req.body.password,
    app.users,
    function(err, user) {
      //assert(!err);
      res.setHeader('content-type', 'application/json');
      if (!user) {
        res.statusCode = 403;
        var o = {message: err.message};
        res.send(JSON.stringify(o));
        return;
      }
      req.session.user = user;
      var fullUrl = req.protocol + '://' + req.get('host') + '/home';
      var o = {message: 'OK', url: fullUrl}
      res.send(JSON.stringify(o));
    });
});

router.post('/logout', function(req, res) {
    req.session.destroy();
    res.redirect('/');
});

////////////////////////////////////////////////////////////////////////////////
// User Profile

router.get('/usr/:username', function(req, res) {
  if (req.session.user == null) {
    // if user is not logged in redirect back to login page
    res.redirect('/');
    return;
  }
  // Check if the user is following a user. I have to get it from the database
  // because otherwise the req.session.user is not uptodate if we just added the user in this session.
  var isFollowing = false;
  app.tweets.find( {username : req.params.username} ).sort({ created_at: -1}).limit(10).toArray(function(err, array){
    app.users.findOne({username : req.params.username}, function(err,profile_user){
      if (profile_user.followers.indexOf(req.session.user.username) >= 0){
        isFollowing = true;
      }
      if (profile_user){
        render(res, {
          following: isFollowing,
          title: profile_user.name,
          username: profile_user.username ,
          partial: 'profile',
          tweets: setDisplayDate(array)
        });
      }else{
        res.send('404: Page not Found', 404);
      }
    });
  });

});

// render users following user req.params.username
router.get('/usr/:username/following', function(req, res) {
  var queryParams = { username : req.params.username }
  app.users.findOne(queryParams, (function(err, user) {
    render(res, {
      title: req.params.username + "is following",
      partial: 'follow',
      follow: user.following
    });
  }))
});

 // render users followed by user req.params.username
router.get('/usr/:username/followers', function(req, res) {
  var queryParams = { username : req.params.username }
  app.users.findOne(queryParams, (function(err, user) {
    render(res, {
      title: req.params.username + "is following",
      partial: 'follow',
      follow: user.followers
    });
  }))
});

// follow a user
router.post('/usr/:username/follow', function(req, res) {
  if (req.session.user == null) {
    res.statusCode = 403;
    var o = {message: "not authenticated"};
    res.send(o);
    return;
  }

  // add the following array for the current logged in user
  app.users.update(
    { username: req.session.user.username},
    { $addToSet: { following:req.params.username } },
    function(err, count, status){
      app.users.update(
        { username: req.params.username },
        { $addToSet: { followers: req.session.user.username} },function(err){
          isFollowing = false
          if (!err){
            isFollowing = true
          }
          res.statusCode = 200;
          res.send({following: isFollowing});
        }
      );
    }
  );

  app.tweets.find( {username : req.params.username} ).toArray(function(err, tweets){
    tweets.forEach(function(tweet){
      app.inbox.save( {username: req.session.user.username,tweet: tweet._id, created_at:tweet.created_at },function(err){
      });
    });
  });

});


// unfollow a user
router.post('/usr/:username/unfollow', function(req, res) {
  if (req.session.user == null) {
    res.statusCode = 403;
    var o = {message: "not authenticated"};
    res.send(o);
    return;
  }
  // remove from the following array for the current logged in user
  app.users.update(
    { username: req.session.user.username},
    { $pull: { following:req.params.username } },
    function(err, count, status){
      app.users.update(
        { username: req.params.username },
        { $pull: { followers: req.session.user.username} },function(err){
          isFollowing = true
          if (!err){
            isFollowing = false
          }
          res.statusCode = 200;
          res.send({following: isFollowing});
        }
      )
    }
  );

  app.tweets.find( {username : req.params.username}, { _id: 1} ).toArray(function(err, tweets){
    tweets_ids = []
    for(var j = 0; j < tweets.length; j++)
    {
      tweets_ids.push(tweets[j]["_id"])
    }
    app.inbox.remove( { username: req.session.user.username,tweet: {$in: tweets_ids}},function(err){
    });
  });

});

////////////////////////////////////////////////////////////////////////////////
// User Timeline

router.get('/home', function(req, res) {
    if (req.session.user == null) {
        // if user is not logged in redirect back to login page
        res.redirect('/');
        return;
    }

    // retrive the last ten tweet for the current users from the people he is following.
    app.inbox.find ({username: req.session.user.username}).limit(10).toArray(function(err, tweets){
      tweets_ids = []
      for(var j = 0; j < tweets.length; j++)
      {
        tweets_ids.push(tweets[j]["tweet"])
      }
      app.tweets.find( { _id: {$in: tweets_ids}}).sort({created_at:-1}).toArray(function(err,tweets_from_db){
        render(res, {
          title: req.session.user.name,
          username: req.session.user.username,
          partial: 'home',
          tweets: setDisplayDate(tweets_from_db)
        });
      });
    });

});

////////////////////////////////////////////////////////////////////////////////
// User Timeline
router.post('/newTweet', function(req, res) {

  // check if the user in authenticated
  if (req.session.user == null) {
      res.statusCode = 403;
      var o = {message: "not authenticated"};
      res.send(o);
      return;
  }
  // check if the tweet text is not empty
  if(req.body.text == ""){
    res.statusCode = 500;
    var o = {message: "cannot send empty tweet"};
    res.send(o);
    return;
  }
  // check if the tweet text lenght is not greater than 140
  if(req.body.text.length > 140){
    res.statusCode = 500;
    var o = {message: "tweets are limited to 140 characters"};
    res.send(o);
    return;
  }

  // Create the new tweet object
  var date = new Date();
  var tweet = {text: req.body.text ,username: req.session.user.username, created_at: date};
  app.users.findOne({username : req.session.user.username}, function(err,user){
    followerCount=0;
    // Insert a new tweets into the tweet database
    app.tweets.save(tweet, function(err, tweet_from_db){
      // Create the message that will be send to kafka
      kafkaMessage = [ { topic: 'twitter-alo', messages: tweet_from_db.text , partition: 0 } ];
      message_mem =  tweet_from_db._id + "|" + tweet_from_db.text
      kafkaMessage_memcached =  [ { topic: 'twitter-alo-memcached', messages: message_mem, partition: 0 } ];
      // if we found the kafka server send the message to it
      if (app.producer_found){
        app.producer.send(kafkaMessage, function (err, data) {
          if(err){
            console.log("kafka error" + err)
          }
        });
        app.producer.send(kafkaMessage_memcached, function (err, data) {
          if(err){
            console.log("kafka error" + err)
          }
        });
      }

      if (user.followers.length < 1){
        var o = {message: "tweet sended"};
        res.statusCode = 200;
        res.send(o);
      }
      // For each follower of a user send a tweet to his inbox
      user.followers.forEach(function(follower) {
          followerCount++;
          app.inbox.save( { username: follower,tweet: tweet_from_db._id ,created_at:tweet.created_at },function(err){
            if (--followerCount === 0) {
              var o = {message: "tweet sended"};
              res.statusCode = 200;
              res.send(o);
            }
          });
      });
    });
  });



});

////////////////////////////////////////////////////////////////////////////////

module.exports = router;
