var fs = require('fs');
var byline = require('byline');
var mongo = require('mongodb').MongoClient;
var assert = require('assert');
var AM = require('./account-manager.js');
var noop = function() {};

mongo.connect('mongodb://localhost:27017/twitter', function(err, db) {

  assert.equal(null, err);
  var lineCount = 0;
  var readAllLines  = false;
  var bucket_size = 10
  users = db.collection('users');
  tweets = db.collection('tweets');
  inbox = db.collection('inbox');

  // Empty the database
  users.remove({}, noop);
  tweets.remove({}, noop);
  inbox.remove({}, noop);

  // Populate tweets into the DB
  function PopulatesTweets() {
    var t = byline(fs.createReadStream(__dirname + '/sample.json'));
    t.on('data', function(line) {
      try {
        var obj = JSON.parse(line);
        // NOTE: obj represents a tweet and contains three fields:
        // obj.created_at: UTC time when this tweet was created
        // obj.text: The actual UTF-8 text of the tweet
        // obj.username: The user who posted this tweet

        // load the tweet into the database
        // decrement the lineCount variable after every insertion
        // if lineCount is equal to zero and readAllLines is true, close db connection
        var tweet = {text: obj.text, username: obj.username, created_at: obj.created_at};

        users.findOne({username : tweet.username}, function(err,profile_user){
          if (!profile_user){
            return
          }
          tweets.insert(tweet, function(err, tweet_from_db){
            profile_user.followers.forEach(function(follower) {
              tweet.to = follower
              lineCount++;
              inbox.insert({ username: follower,tweet: tweet_from_db._id, created_at:tweet.created_at },function(err){
                if (--lineCount === 0) {
                  db.close();
                  console.log("tweets inserted")
                }
              });

            });
          });

        });

        } catch (err) {
          console.log("Error:", err);
        }
      });
    }

  // Populate users into the DB
  var u = byline(fs.createReadStream(__dirname + '/users.json'));
  users_to_save = []
  // Test user
  var user = {username: "alo", name: "alois", following: [], pass: "alo", followers: []};
  AM.addNewAccount(user, users,function(){
    console.log("test user added")
  });

  u.on('data', function(line) {
    try {
      var obj = JSON.parse(line);
      // NOTE: obj represents a user and contains three fields:
      // obj.username: the username
      // obj.name: the full name
      // obj.followers: array of users following this user
      // obj.following: array of followed users

      // load the user into the database
      // (including the information of which users this user is following)
      // decrement the lineCount variable after every insertion
      // if lineCount is equal to zero and readAllLines is true, close db connection
      var user = {username: obj.username,name: obj.name, following: obj.following, followers: obj.followers};
      users_to_save.push(user)
    } catch (err) {
      console.log("Error:", err);
    }
  });

  u.on('end', function(){
    // Insert every user in one request, to ensure they are all added
    // because when you close the connection to the DB after reading all the data
    // some users might not have been saved
    users.insert(users_to_save, { safe: true }, function (error, inserted) {
      if (error){
        console.log(error)
      }
      console.log("user inserted, start inserting tweet");
      PopulatesTweets();
    });
  });


});
