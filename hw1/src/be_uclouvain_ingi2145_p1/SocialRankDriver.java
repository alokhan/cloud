
// Alois Paulus 
package be_uclouvain_ingi2145_p1;

import java.io.Console;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.text.DecimalFormat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.io.WritableComparable;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.varia.LevelRangeFilter;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;


public class SocialRankDriver extends Configured implements Tool
{
  /**
  * Singleton instance.
  */
  public static SocialRankDriver GET;

  /**
  * Number of iterations to perform in between diff checks.
  */
  public static final double DIFF_INTERVAL = 1;

  public static final double d = 0.15;

  /**
  * Author's name.
  */
  public static final String NAME = "Alois Paulus";

  // ---------------------------------------------------------------------------------------------

  public static void main(String[] args) throws Exception
  {
    // configure log4j to output to a file
    Logger logger = LogManager.getRootLogger();
    logger.addAppender(new FileAppender(new SimpleLayout(), "p1.log"));

    // configure log4j to output to the console
    Appender consoleAppender = new ConsoleAppender(new SimpleLayout());
    LevelRangeFilter filter = new LevelRangeFilter();
    // switch to another level for more detail (own (INGI2145) messages use FATAL)
    filter.setLevelMin(Level.ERROR);
    consoleAppender.addFilter(filter);
    // (un)comment to (un)mute console output
    logger.addAppender(consoleAppender);

    // switch to Level.DEBUG or Level.TRACE for more detail
    logger.setLevel(Level.INFO);

    GET = new SocialRankDriver();
    Configuration conf = new Configuration();

    int res = ToolRunner.run(conf, GET, args);
    System.exit(res);
  }

  // ---------------------------------------------------------------------------------------------

  @Override
  public int run(String[] args) throws Exception
  {
    System.out.println(NAME);

    if (args.length == 0) {
      args = new String[]{ "command missing" };
    }

    switch (args[0]) {
      case "init":
      init(args[1], args[2], Integer.parseInt(args[3]));
      break;
      case "iter":
      iter(args[1], args[2], Integer.parseInt(args[3]));
      break;
      case "diff":
      diff(args[1], args[2], args[3], args[4], Integer.parseInt(args[5]));
      break;
      case "finish":
      finish(args[1], args[2], args[3], Integer.parseInt(args[4]));
      break;
      case "composite":
      composite(args[1], args[2], args[3], args[4], args[5],
      args[6], Double.parseDouble(args[7]), Integer.parseInt(args[8]));
      break;
      default:
      System.out.println("Unknown command: " + args[0]);
      break;
    }

    return 0;
  }

  // ---------------------------------------------------------------------------------------------

  void init(String inputDir, String outputDir, int nReducers) throws Exception
  {
    Job init = Utils.configureJob(inputDir, outputDir, InitMap.class, null, InitReduce.class, IntWritable.class, Text.class, IntWritable.class, Text.class, nReducers);

    Utils.startJob(init);
  }

  public static class InitMap extends Mapper<LongWritable, Text, IntWritable, Text>
  {
    // Parse the input format and split it with the help of tab
    @Override
    protected void map(LongWritable key, Text value, Context context)
    throws IOException, InterruptedException
    {
      String line = value.toString();
      String[] numbers = line.split("\t");
      context.write(new IntWritable(Integer.parseInt(numbers[0])), new Text(numbers[1]));
    }
  }

  public static class InitReduce extends Reducer<IntWritable, Text, IntWritable, Text>
  {
    // Produce the intermediate format
    @Override
    protected void reduce(IntWritable key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException
    {
      // Initial rank
      String line = "1.0 ";
      ArrayList<String> myFriends = new ArrayList<String>();
      for (Text v : values) {
        line += v.toString() + " ";
      }
      // Write a vertex, his initial rank and the list of his friends
      context.write(key, new Text(line));
    }
  }

  // ---------------------------------------------------------------------------------------------

  void iter(String inputDir, String outputDir, int nReducers) throws Exception
  {

    Job iter = Utils.configureJob(inputDir, outputDir, IterMap.class, null, IterReduce.class, IntWritable.class, Text.class, IntWritable.class, Text.class, nReducers);
    Utils.startJob(iter);
  }

  public static class IterMap extends Mapper<LongWritable, Text, IntWritable, Text>
  {

    // Read the intermediate format. This can come from the init step or a previous excecution of iter step.
    @Override
    protected void map(LongWritable key, Text value, Context context)
    throws IOException, InterruptedException
    {
      String line = value.toString();
      String[] lineArray = line.split("\t");
      String people = lineArray[0];
      lineArray = lineArray[1].split(" ");
      String rank = lineArray[0];

      // check if this node has no friend otherwise array out of bound
      if (lineArray.length < 2){
        return;
      }

      String[] friends =  Arrays.copyOfRange(lineArray,1,lineArray.length);
      int friendsCount = friends.length;
      // For each friend of X
      // Lets output "friend X friendRank NbFriendsOfFriend"
      for (String friend : friends){
        Text newLine = new Text(people + " " + rank + " " + friendsCount);
        context.write(new IntWritable(Integer.parseInt(friend)), newLine);
      }

      // Construct friends list
      String friendOutput = "";
      for (String f : friends) {
        friendOutput += f + " ";
      }
      // at the end add the vertex and his friends list so we keep all the information needed.
      context.write(new IntWritable(Integer.parseInt(people)), new Text("#"+friendOutput));
    }
  }

  public static class IterReduce extends Reducer<IntWritable, Text, IntWritable, Text>
  {
    @Override
    protected void reduce(IntWritable key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException
    {

      String friends = "";
      double sumOfFriends = 0;

      // For each ppl which have X has friend.
      for (Text val : values) {
        String line = val.toString();

        // if we are on the line containing all the friends for the key
        if (line.startsWith("#")){
          friends += line.substring(1);
          // else we are on a line which contains info to compute rank
        }else{
          String[] lineArray = line.split(" ");
          String rank = lineArray[1];
          String friendCount = lineArray[2];
          sumOfFriends += (Double.parseDouble(rank) / Double.parseDouble(friendCount));
        }

      }
      // Compute new rank with the help of the previously computed value for people which has X has friend.
      double newRank = d + ((1.0 - d) * sumOfFriends);
      if (key.get() == 852){
         System.out.println("852 " + newRank + " " + friends);
      }
      context.write(key, new Text(newRank + " " + friends));
    }
  }

  // ---------------------------------------------------------------------------------------------

  double diff(String inputDir1, String inputDir2, String tmpDir, String outputDir, int nReducers)
  throws Exception
  {
    Job diff = Utils.configureJob(inputDir1, tmpDir, DiffMap.class, null, DiffReduce.class, IntWritable.class, DoubleWritable.class, DoubleWritable.class, IntWritable.class, nReducers);
    // add the second input directory
    FileInputFormat.addInputPath(diff, new Path(inputDir2));

    Utils.startJob(diff);

    // Job to produce a single diff file. For that I only use one reducer.
    Job diffToOneFile = Utils.configureJob(tmpDir, outputDir, DiffMapOne.class, null, DiffReduceOne.class, DoubleWritable.class, DoubleWritable.class,DoubleWritable.class, Text.class, 1);

    // for the decreasing order
    diffToOneFile.setSortComparatorClass(LongWritable.DecreasingComparator.class);

    Utils.startJob(diffToOneFile);

    double diffResult = Utils.readDiffResult(outputDir);
    System.out.println("diff :" + diffResult);
    return diffResult;
  }


  // First diff map/reduce to compute the difference using multiple reducers
  public static class DiffMap extends Mapper<LongWritable, Text, IntWritable, DoubleWritable>
  {

    @Override
    protected void map(LongWritable key, Text value, Context context)
    throws IOException, InterruptedException
    {
      String line = value.toString();
      String[] numbers = line.split("\t");
      context.write(new IntWritable(Integer.parseInt(numbers[0])), new DoubleWritable(Double.parseDouble(numbers[1].split(" ")[0])));
    }
  }


  public static class DiffReduce extends Reducer<IntWritable, DoubleWritable, DoubleWritable, IntWritable>
  {
    @Override
    protected void reduce(IntWritable key, Iterable<DoubleWritable> values, Context context)
    throws IOException, InterruptedException
    {
      // Initial rank
      double diff = -1;
      int i = 0;
      for (DoubleWritable v : values) {
        if (diff == -1){
          diff = v.get();
        }else{
          diff = diff - v.get();
        }
        i++;
      }
      diff = Math.abs(diff);
      context.write(new DoubleWritable(diff), null);
    }
  }


  // Second diff Map/Reduce to merge all the computed differences into one file
  public static class DiffMapOne extends Mapper<LongWritable, Text, DoubleWritable, DoubleWritable>
  {

    @Override
    protected void map(LongWritable key, Text value, Context context)
    throws IOException, InterruptedException
    {
      String line = value.toString();
      context.write(new DoubleWritable(Double.parseDouble(line)), new DoubleWritable(1.0));
    }
  }

  public static class DiffReduceOne extends Reducer<DoubleWritable, DoubleWritable, DoubleWritable, Text>
  {
    @Override
    protected void reduce(DoubleWritable key, Iterable<DoubleWritable> values, Context context)
    throws IOException, InterruptedException
    {
      context.write(key, new Text(" "));
    }
  }

  // ---------------------------------------------------------------------------------------------

  void finish(String inputDir, String tmpDir, String outputDir, int nReducers) throws Exception
  {
    Job finish = Utils.configureJob(inputDir, tmpDir, FinishMap.class, null, FinishReduce.class, DoubleWritable.class, IntWritable.class, Text.class, IntWritable.class, nReducers);

    Utils.startJob(finish);

    Job SecondFinish = Utils.configureJob(tmpDir, outputDir, SecondFinishMap.class, null, SecondFinishReduce.class, DoubleWritable.class, Text.class, Text.class, Text.class, 1);
    SecondFinish.setSortComparatorClass(LongWritable.DecreasingComparator.class);
    Utils.startJob(SecondFinish);

    Logger.getRootLogger().fatal("[INGI2145] finish from:" + inputDir);
  }

  // FinishMap use by the first finish job which can be run by multiple reducer.
  // Select only needed value from input format.
  public static class FinishMap extends Mapper<LongWritable, Text, DoubleWritable, IntWritable>
  {

    @Override
    protected void map(LongWritable key, Text value, Context context)
    throws IOException, InterruptedException
    {
      String line = value.toString();
      String[] numbers = line.split("\t");
      String newKey = numbers[1].split(" ")[0];
      context.write(new DoubleWritable(Double.parseDouble(newKey)), new IntWritable(Integer.parseInt(numbers[0])));
    }
  }

  // FinishReduce use by the first finish job which can be run by multiple reducer.
  // Write only needed value to output format
  public static class FinishReduce extends Reducer<DoubleWritable, IntWritable, Text, IntWritable>
  {
    @Override
    protected void reduce(DoubleWritable key, Iterable<IntWritable> values, Context context)
    throws IOException, InterruptedException
    {
      for (IntWritable val : values){
        context.write(new Text(key.toString()), val);
      }
    }
  }

  // SecondFinishMap used by the second finish job which can only be run by one reducer.
  public static class SecondFinishMap extends Mapper<LongWritable, Text, DoubleWritable, Text>
  {

    @Override
    protected void map(LongWritable key, Text value, Context context)
    throws IOException, InterruptedException
    {
      String line = value.toString();
      String[] numbers = line.split("\t");
      context.write(new DoubleWritable(Double.parseDouble(numbers[0])), new Text(numbers[1]));
    }
  }
  // SecondFinishReduce used by the second finish job which can only be run by one reducer.
  // Write one file which the format "vertex rank"
  public static class SecondFinishReduce extends Reducer<DoubleWritable, Text, Text, Text>
  {
    @Override
    protected void reduce(DoubleWritable key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException
    {
      for (Text val : values){
        context.write(val, new Text(key.toString()));
      }
    }
  }

  // ---------------------------------------------------------------------------------------------

  // Will run the iter step until the diff is smaller than the delta
  // then will run the finish job
  void composite(String inputDir, String outputDir, String intermDir1, String intermDir2,
  String diffDir, String tmpDiffDir, double delta, int nReducers) throws Exception
  {
    init(inputDir,intermDir1,nReducers);

    String dirA = intermDir1;
    String dirB = intermDir2;

    iter(dirA,dirB,nReducers);
    int u =0;
    while (diff(dirA,dirB,tmpDiffDir,diffDir,nReducers) > delta){
      for (int i = 0 ; i <= DIFF_INTERVAL ; i++){
        String temp = dirA;
        dirA = dirB;
        dirB = temp;
        iter(dirA,dirB,nReducers);
      }
      System.out.println("nb of diff: " + u);
      u++;
    }

    finish(dirB, tmpDiffDir, outputDir, nReducers);
  }


}
